import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../../services/products.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css'],
  providers: [ProductsService]
})
export class ProductsComponent implements OnInit {

  	tr: any;
  	products: any;
  	
    constructor(private httpService : ProductsService)
    {     	
    }
    
	ngOnInit() 
  	{
  		this.init(); 	
  	}
  	
  	paginate(event)
 	{   
    	this.getDataForPage(event.page);
   	}
      
    init()
    {  
    	this.httpService.getdata(1).subscribe(response =>{
				if(response.error) { 
					alert('Server Error');
				} else 
				{								
					this.products = response.content;				
					this.tr = response.totalElements;
								
				}
			},
			error =>{
				alert('Server error');
			}
		);
    }
    
    getDataForPage(page)
    {
		this.httpService.getdata(page + 1).subscribe(
			response =>{
				if(response.error) { 
					alert('Server Error');
				} else {								
					this.products = response.content;				
					this.tr = response.totalElements;							
				}
			},
			error =>{
				alert('Server error');
			}
		);    
    }

}
