import { Component, OnInit } from '@angular/core';
import { ProducttypesService } from '../../services/producttypes.service';

@Component({
  selector: 'app-producttypes',
  templateUrl: './producttypes.component.html',
  styleUrls: ['./producttypes.component.css'],
  providers: [ProducttypesService]
})
export class ProducttypesComponent implements OnInit {

    tr: any;
  	producttypes: any;
  	
    constructor(private httpService : ProducttypesService)
    {     	
    }
    
	ngOnInit() 
  	{
  		this.init(); 	
  	}
  	
  	paginate(event)
 	{   
    	this.getDataForPage(event.page);
   	}
      
    init()
    {  
    	this.httpService.getdata(1).subscribe(response =>{
				if(response.error) { 
					alert('Server Error');
				} else 
				{								
					this.producttypes = response.content;				
					this.tr = response.totalElements;
								
				}
			},
			error =>{
				alert('Server error');
			}
		);
    }
    
    getDataForPage(page)
    {
		this.httpService.getdata(page + 1).subscribe(
			response =>{
				if(response.error) { 
					alert('Server Error');
				} else {								
					this.producttypes = response.content;				
					this.tr = response.totalElements;							
				}
			},
			error =>{
				alert('Server error');
			}
		);    
    }


}
