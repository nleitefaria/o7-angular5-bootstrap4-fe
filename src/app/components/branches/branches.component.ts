import { Component, OnInit } from '@angular/core';
import { BranchesService } from '../../services/branches.service';

@Component({
  selector: 'app-branches',
  templateUrl: './branches.component.html',
  styleUrls: ['./branches.component.css'],
  providers: [BranchesService]
})
export class BranchesComponent implements OnInit {

    tr: any;
  	branches: any;
  	
    constructor(private httpService : BranchesService)
    {     	
    }
    
	ngOnInit() 
  	{
  		this.init(); 	
  	}
  	
  	paginate(event)
 	{   
    	this.getDataForPage(event.page);
   	}
      
    init()
    {  
    	this.httpService.getdata(1).subscribe(response =>{
				if(response.error) { 
					alert('Server Error');
				} else 
				{								
					this.branches = response.content;				
					this.tr = response.totalElements;
								
				}
			},
			error =>{
				alert('Server error');
			}
		);
    }
    
    getDataForPage(page)
    {
		this.httpService.getdata(page + 1).subscribe(
			response =>{
				if(response.error) { 
					alert('Server Error');
				} else {								
					this.branches = response.content;				
					this.tr = response.totalElements;							
				}
			},
			error =>{
				alert('Server error');
			}
		);    
    }

}
