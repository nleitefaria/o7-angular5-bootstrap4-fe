import { Component, OnInit } from '@angular/core';
import { IndividualsService } from '../../services/individuals.service';

@Component({
  selector: 'app-individuals',
  templateUrl: './individuals.component.html',
  styleUrls: ['./individuals.component.css'],
  providers: [IndividualsService]
})

export class IndividualsComponent implements OnInit {

  	tr: any;
  	individuals: any;
  	
    constructor(private httpService : IndividualsService)
    {     	
    }
    
	ngOnInit() 
  	{
  		this.init(); 	
  	}
  	
  	paginate(event)
 	{   
    	this.getDataForPage(event.page);
   	}
      
    init()
    {  
    	this.httpService.getdata(1).subscribe(response =>{
				if(response.error) 
				{ 
					alert('Server Error');
				} 
				else 
				{								
					this.individuals = response.content;				
					this.tr = response.totalElements;								
				}
			},
			error =>{
				alert('Server error');
			}
		);
    }
    
    getDataForPage(page)
    {
		this.httpService.getdata(page + 1).subscribe(
			response =>{
				if(response.error) { 
					alert('Server Error');
				} else {								
					this.individuals = response.content;				
					this.tr = response.totalElements;							
				}
			},
			error =>{
				alert('Server error');
			}
		);    
    }
}
