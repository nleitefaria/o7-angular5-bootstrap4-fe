import { Component, OnInit } from '@angular/core';
import { AcctransactionsService } from '../../services/acctransactions.service';

@Component({
  selector: 'app-acctransactions',
  templateUrl: './acctransactions.component.html',
  styleUrls: ['./acctransactions.component.css'],
  providers: [AcctransactionsService]
})

export class AcctransactionsComponent implements OnInit
{
    tr: any;
  	acctransactions: any;
  	
    constructor(private httpService : AcctransactionsService)
    {     	
    }
    
	ngOnInit() 
  	{
  		this.init(); 	
  	}
  
  	onclick()
  	{  
 	}
    
	paginate(event)
 	{   
    	this.getDataForPage(event.page);
   	}
      
    init()
    {  
    	this.httpService.getdata(1).subscribe(response =>{
				if(response.error) { 
					alert('Server Error');
				} else 
				{								
					this.acctransactions = response.content;				
					this.tr = response.totalElements;
								
				}
			},
			error =>{
				alert('Server error');
			}
		);
    }
    
    getDataForPage(page)
    {
		this.httpService.getdata(page + 1).subscribe(
			response =>{
				if(response.error) { 
					alert('Server Error');
				} else {								
					this.acctransactions = response.content;				
					this.tr = response.totalElements;							
				}
			},
			error =>{
				alert('Server error');
			}
		);    
    }

}
