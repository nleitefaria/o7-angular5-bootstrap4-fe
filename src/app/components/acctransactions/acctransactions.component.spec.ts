import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AcctransactionsComponent } from './acctransactions.component';

describe('AcctransactionsComponent', () => {
  let component: AcctransactionsComponent;
  let fixture: ComponentFixture<AcctransactionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AcctransactionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AcctransactionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
