import { Component, OnInit } from '@angular/core';
import { EmployeesService } from '../../services/employees.service';

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.css'],
  providers: [EmployeesService]
})

export class EmployeesComponent implements OnInit 
{
    tr: any;
  	employees: any;
  	
    constructor(private httpService : EmployeesService)
    {     	
    }
    
	ngOnInit() 
  	{
  		this.init(); 	
  	}
  	
  	paginate(event)
 	{   
    	this.getDataForPage(event.page);
   	}
      
    init()
    {  
    	this.httpService.getdata(1).subscribe(response =>{
				if(response.error) 
				{ 
					alert('Server Error');
				} 
				else 
				{								
					this.employees = response.content;				
					this.tr = response.totalElements;								
				}
			},
			error =>{
				alert('Server error');
			}
		);
    }
    
    getDataForPage(page)
    {
		this.httpService.getdata(page + 1).subscribe(
			response =>{
				if(response.error) { 
					alert('Server Error');
				} else {								
					this.employees = response.content;				
					this.tr = response.totalElements;							
				}
			},
			error =>{
				alert('Server error');
			}
		);    
    }


}
