import { Component, OnInit } from '@angular/core';
import { BusinessesService } from '../../services/businesses.service';

@Component({
  selector: 'app-businesses',
  templateUrl: './businesses.component.html',
  styleUrls: ['./businesses.component.css'],
  providers: [BusinessesService]
})
export class BusinessesComponent implements OnInit {

  	tr: any;
  	businesses: any;
  	
    constructor(private httpService : BusinessesService)
    {     	
    }
    
	ngOnInit() 
  	{
  		this.init(); 	
  	}
  	
  	paginate(event)
 	{   
    	this.getDataForPage(event.page);
   	}
      
    init()
    {  
    	this.httpService.getdata(1).subscribe(response =>{
				if(response.error) { 
					alert('Server Error');
				} else 
				{								
					this.businesses = response.content;				
					this.tr = response.totalElements;
								
				}
			},
			error =>{
				alert('Server error');
			}
		);
    }
    
    getDataForPage(page)
    {
		this.httpService.getdata(page + 1).subscribe(
			response =>{
				if(response.error) { 
					alert('Server Error');
				} else {								
					this.businesses = response.content;				
					this.tr = response.totalElements;							
				}
			},
			error =>{
				alert('Server error');
			}
		);    
    }

}
