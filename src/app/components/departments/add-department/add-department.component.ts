import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms'; 
import { Router } from '@angular/router';
import { DepartmentsService } from '../../../services/departments.service'; 

@Component({
  selector: 'app-add-department',
  templateUrl: './add-department.component.html',
  styleUrls: ['./add-department.component.css'],
  providers: [DepartmentsService]
})

export class AddDepartmentComponent implements OnInit 
{
	department =
	{
       name:''
   	}

  	constructor(private httpService : DepartmentsService, private router: Router) 
  	{ 
  	}

  	ngOnInit() 
  	{
  	}
  
  	onSubmit({value, valid})
  	{
    	if(valid)
    	{  		
			this.httpService.postData(value.name).subscribe(response =>{
				if(response.error)
				{ 
					alert('Server Error');
				} 
				else 
				{
					alert('Sucess');								
					this.router.navigate(['/departments']);
								
				}
			},
			error =>{
				alert('Server error');
			}
			);        
    	} 
    	else 
    	{
        	console.log('Form is invalid');
    	}
   	}

}
