import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router'; 
import { DepartmentsService } from '../../../services/departments.service';

@Component({
  selector: 'app-edit-department',
  templateUrl: './edit-department.component.html',
  styleUrls: ['./edit-department.component.css'],
  providers: [DepartmentsService]
})

export class EditDepartmentComponent implements OnInit 
{
  	id: number;
  	department: any;
  
  	constructor(private httpService : DepartmentsService, private route: ActivatedRoute, private router: Router) 
  	{
  		this.route.params.subscribe((params) => 
  		{
  			this.id = params.id; 
  			alert(this.id);				
		});	
  	}

  	ngOnInit()
  	{
  		this.department = null;
  		this.init(); 	
  	}
  	
  	init()
    {  
    	this.httpService.getOne(this.id).subscribe(response =>{
				if(response.error) { 
					alert('Server Error');
				} else 
				{								
					this.department = response;											
				}
			},
			error =>{
				alert('Server error');
			}
		);
    }
    
    onSubmit(id: Number, {value, valid})
  	{
  		alert("deptId: " + id);
  		alert("value: " + value);
  		alert("value.name: " + value.name);
    	if(valid)
    	{  	
    			
			this.httpService.putData(id, value.name).subscribe(response =>{
				/*
				if(response.error)
				{ 
					alert('Server Error');
				} 
				else 
				{
					alert('Sucess');								
					this.router.navigate(['/departments']);
								
				}
				*/
				alert(response);
				this.router.navigate(['/departments']);
			},
			error =>{
				alert('Server error');
			}
			);
			
			
			        
    	} 
    	else 
    	{
        	console.log('Form is invalid');
    	}
   	}

}
