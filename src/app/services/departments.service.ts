import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { environment } from '../../environments/environment';

@Injectable()
export class DepartmentsService {

  	private BASE_URL1 : string = environment.apiUrl + 'departments/';
  	private BASE_URL2 : string = environment.apiUrl + 'department/';
	
	constructor(private http:Http)
	{
	}
 
	public getData(page:Number):any{
		return this.http.get(`${this.BASE_URL1}page/${page}`)
			.map((response:Response) => response.json())
			.catch((error:any) => Observable.throw(error.json().error) || 'Server Error');
	}
	
	public getOne(id:Number):any{
		return this.http.get(`${this.BASE_URL2}${id}`)
			.map((response:Response) => response.json())
			.catch((error:any) => Observable.throw(error.json().error) || 'Server Error');
	}
	
	public postData(name:string):any{
		return this.http.post(`${this.BASE_URL2}`, {name:name})
			.map((response:Response) => response.json())
			.catch((error:any) => Observable.throw(error.json().error) || 'Server Error');
	}
	
	public putData(id:Number, name:string):any{
		alert(id + " " + name );
		return this.http.put(`${this.BASE_URL2}${id}`, {name:name})
			.map((response:Response) => response.json())
			.catch((error:any) => Observable.throw(error.json().error) || 'Server Error');
	}

}
