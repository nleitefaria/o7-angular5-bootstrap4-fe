import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { environment } from '../../environments/environment';

@Injectable()
export class ProductsService {

  	private BASE_URL:string = environment.apiUrl + 'products/';
	
	constructor(private http:Http)
	{
	}
 
	public getdata(page:Number):any{
		return this.http.get(`${this.BASE_URL}page/${page}`)
			.map((response:Response) => response.json())
			.catch((error:any) => Observable.throw(error.json().error) || 'Server Error');
	}

}
