import { TestBed, inject } from '@angular/core/testing';

import { AcctransactionsService } from './acctransactions.service';

describe('AcctransactionsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AcctransactionsService]
    });
  });

  it('should be created', inject([AcctransactionsService], (service: AcctransactionsService) => {
    expect(service).toBeTruthy();
  }));
});
