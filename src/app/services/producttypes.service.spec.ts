import { TestBed, inject } from '@angular/core/testing';

import { ProducttypesService } from './producttypes.service';

describe('ProducttypesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProducttypesService]
    });
  });

  it('should be created', inject([ProducttypesService], (service: ProducttypesService) => {
    expect(service).toBeTruthy();
  }));
});
