import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms'

import { AppRoutingModule } from './app-routing.module';

import { ButtonModule } from 'primeng/primeng';
import { PaginatorModule } from 'primeng/primeng';

import { AppComponent } from './app.component';
import { BranchesComponent } from './components/branches/branches.component';
import { AcctransactionsComponent } from './components/acctransactions/acctransactions.component';
import { AccountsComponent } from './components/accounts/accounts.component';
import { ProductsComponent } from './components/products/products.component';
import { ProducttypesComponent } from './components/producttypes/producttypes.component';
import { BusinessesComponent } from './components/businesses/businesses.component';
import { CustomersComponent } from './components/customers/customers.component';
import { DepartmentsComponent } from './components/departments/departments.component';
import { EmployeesComponent } from './components/employees/employees.component';
import { IndividualsComponent } from './components/individuals/individuals.component';
import { AddDepartmentComponent } from './components/departments/add-department/add-department.component';
import { EditDepartmentComponent } from './components/departments/edit-department/edit-department.component';



@NgModule({
  declarations: [
    AppComponent,
    BranchesComponent,
    AcctransactionsComponent,
    AccountsComponent,
    ProductsComponent,
    ProducttypesComponent,
    BusinessesComponent,
    CustomersComponent,
    DepartmentsComponent,
    EmployeesComponent,
    IndividualsComponent,
    AddDepartmentComponent,
    EditDepartmentComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpModule,  
    FormsModule,  
    ButtonModule,
    PaginatorModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
