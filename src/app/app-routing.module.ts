import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AccountsComponent } from './components/accounts/accounts.component';
import { BranchesComponent } from './components/branches/branches.component';
import { AcctransactionsComponent } from './components/acctransactions/acctransactions.component';
import { ProductsComponent } from './components/products/products.component';
import { ProducttypesComponent } from './components/producttypes/producttypes.component';
import { BusinessesComponent } from './components/businesses/businesses.component';
import { EmployeesComponent } from './components/employees/employees.component';
import { IndividualsComponent } from './components/individuals/individuals.component';
import { CustomersComponent } from './components/customers/customers.component';
import { DepartmentsComponent } from './components/departments/departments.component';
import { AddDepartmentComponent } from './components/departments/add-department/add-department.component';
import { EditDepartmentComponent } from './components/departments/edit-department/edit-department.component';

const routes: Routes = [
	{
		path: 'accounts',
		component: AccountsComponent
	},
	{
		path: 'branches',
		component: BranchesComponent
	},
	{
		path: 'acctransactions',
		component: AcctransactionsComponent
	},
	{
		path: 'products',
		component: ProductsComponent
	},
	{
		path: 'producttypes',
		component: ProducttypesComponent
	},
	{
		path: 'businesses',
		component: BusinessesComponent
	},
	{
		path: 'employees',
		component: EmployeesComponent
	},
	{
		path: 'individuals',
		component: IndividualsComponent
	},
	{
		path: 'customers',
		component: CustomersComponent
	},
	{
		path: 'departments',
		component: DepartmentsComponent
	},
	{
		path: 'edit-department/:id',
		component: EditDepartmentComponent
	},
	{
		path: 'add-department',
		component: AddDepartmentComponent
	}
	
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
